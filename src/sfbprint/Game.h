#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

class Game
{
	public:
	//the delete part es una forma de c++11 de
	//para borrar algun mienbro o constructoy y evitar copas o para crear algun
	//constructor por defecto
		Game(const Game&) = delete;
		Game& operator = (const Game&) = delete;
		Game();//constructor
		void run();//metodo que corre el motor

	private:
		void processEvents();//eventos inputs
		void update();//se ejecuta cada frame y actualiza los estados
		void render();//dibuja los objetos

		sf::RenderWindow _window;//un objeto ventana
		sf::CircleShape _player;//un circulo que representa al player
};