#include "Game.h"

//inicializa la ventana con un objeto player de tamaño 150
//ese es un circulo y dentro de la funcion game imoemente se agrega
//el color interno y la posiciónen el lienzo
Game::Game():_window(sf::VideoMode(1280,720), "Game Test"), _player(150)
{
	_player.setFillColor(sf::Color::Blue);
	_player.setPosition(10, 20);
}

void Game::run()//el run es el gameloop
{
	while (_window.isOpen())
	{
		///
		processEvents();
		update();
		render();
	}
}

void Game::processEvents()
{
	sf::Event event;
	while(_window.pollEvent(event))
	{
		if((event.type == sf::Event::Closed)
		or ((event.type == sf::Event::KeyPressed)
		 and (event.key.code==sf::Keyboard::Escape)))
		 {
			 _window.close();
		 }
	}
}

void Game::update(){}

void Game::render()
{
	_window.clear();
	_window.draw(_player);
	_window.display();
}